import express from 'express'
import axios from 'axios'

const app = express()

app.get('/', async (req, res) => {
  try {

    const response = await axios.get(
      req.query.image,
      { responseType: 'arraybuffer' }
    );
    const b64 = Buffer.from(response.data).toString('base64');
    const mimeType = 'image/png'; // e.g., image/png

    const script = `
      <html>
        <body>
          <img src="data:${mimeType};base64,${b64}" />
        </body>
      </html>
    `
    res.send(script)
  } catch (error) {
    console.log(error)
    res.status(400).json({
      message: 'error'
    })
  }
})

app.listen(8080, () => {
  console.log('Server listen in port 8080')
})

